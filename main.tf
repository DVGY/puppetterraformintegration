terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
       version = "= 4.27.0"
    }
  }
}


provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "instance1" {

ami = data.aws_ami.ubuntu.id

instance_type = "t2.micro"

vpc_security_group_ids = [aws_security_group.main.id]

key_name = "aws_key"

tags = {

Name = "tf1"

}

provisioner "remote-exec" {
    script = "./puppet.sh"
}
connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file("C:/key/key")
      timeout     = "4m"
}

depends_on = [

  aws_key_pair.devkey

]

}


resource "aws_security_group" "main" {
  egress = [
    {
      cidr_blocks      = [ "0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
  ]
 ingress                = [
   {
     cidr_blocks      = [ "0.0.0.0/0", ]
     description      = ""
     from_port        = 22
     ipv6_cidr_blocks = []
     prefix_list_ids  = []
     protocol         = "tcp"
     security_groups  = []
     self             = false
     to_port          = 22
  }
  ]
}

resource "aws_key_pair" "devkey" {
  key_name   = "aws_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDiGAaobN7VOopG1q/i67+0KadvzkAULosE+fZwkt2+ZIauqexWYKSm1KJE/6B4KKSSg4/y0s7MEEq6N2R4KauxWSA+FoSME9SQR6/TCOisMo3vQoKddV7nAo3/Ye04OpSdunoKKJyOZLmxXe2hpJ+OnnlavPxX0pLpQRKrkTaNX9eKhZoUpiEvps83XGuSYL0SBjRXkoEFJ4ERST3uMvLnJCHbe15Y5NaKe//+3FG2xCrzqaoySH/dxLCxWvvuOBEwb3W/oDAC5QyIaTS05cGeQHBzKU/h7sludx3zgPQi4ibQiH+Vm1AwB5aIenmzmxzJ8OOeMB1gvGK43w5D2+vb dyans@LAPTOP-NV2Q628E"
}