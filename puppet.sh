#!/bin/sh
sudo apt-get update && sudo apt-get install ruby -y
wget https://apt.puppetlabs.com/puppet8-release-jammy.deb
sudo dpkg -i puppet8-release-jammy.deb
sudo apt update -y 
sudo apt install puppetserver -y
sudo sed -i -e 's/\-Xms2g \-Xmx2g / \-Xms250m \-Xmx250m /g' /etc/default/puppetserver
sudo systemctl start puppetserver